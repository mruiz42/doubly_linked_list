#pragma once
template <class T>
class List
{
public:
	List();
	~List();
	Node<T>* get_top() { return top; }
	int add();
	int remove();

private:
	Node<T>* top;
	int count;


};

