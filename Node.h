#pragma once
template <class T>
class Node
{
public:

	Node<T>() : data(NULL), next(nullptr) {}						// Default constructor		
	Node<T>(T d, Node<T>* ptr = nullptr) : data(d), next(ptr) {}	// Constructor
	//~Node<T>() : data(NULL), next(nullptr) {}

	T get_data() const { return data; }
	int set_data(T d) { data = d; }

	Node<T>* get_next() const { return next; }
	int set_next(Node<T>* n) { next = n; }
private:
	T data;
	Node<T>* next;
};

